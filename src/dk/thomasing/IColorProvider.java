package dk.thomasing;

public interface IColorProvider {
    public String getRandomColor();
}
