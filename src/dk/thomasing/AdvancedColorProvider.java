package dk.thomasing;

public class AdvancedColorProvider implements IColorProvider{
    private final String[] colors = {"RED", "BLUE", "GREEN"};

    @Override
    public String getRandomColor() {
        int i = (int)(Math.random()*3);
        return colors[i];
    }
}
